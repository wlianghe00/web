package com.yixiubaby;

import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.igexin.sdk.PushConsts;
import com.yixiubaby_ls.R;

/**
 * 推送接收
 * @author wlh
 */
public class PushReceiver extends BroadcastReceiver {
	NotificationManager manager;

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		Log.d("GetuiSdkDemo", "onReceive() action=" + bundle.getInt("action"));
		switch (bundle.getInt(PushConsts.CMD_ACTION)) {

		case PushConsts.GET_MSG_DATA:
			// 获取透传数据
			// String appid = bundle.getString("appid");
			byte[] payload = bundle.getByteArray("payload");

			if (payload != null) {
				String data = new String(payload);
				Log.d("GetuiSdkDemo", "Got Payload:" + data);
				Intent intentTo = new Intent(context, DetailActivity.class);
				intentTo.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				String[] strs = data.split("&");
				if(strs != null && strs.length >= 2) {
					String content = strs[0];
					String url = strs[1];
					intentTo.putExtra("url", url);
					showNotification(context, content, intentTo, null);
				}
			}
			break;
		case PushConsts.GET_CLIENTID:
			// 获取ClientID(CID)
			// 第三方应用需要将CID上传到第三方服务器，并且将当前用户帐号和CID进行关联，以便日后通过用户帐号查找CID进行消息推送
			String cid = bundle.getString("clientid");
			Log.d("GetuiSdkDemo", cid);
			break;
		case PushConsts.THIRDPART_FEEDBACK:
			String appid = bundle.getString("appid");
			String taskid = bundle.getString("taskid");
			String actionid = bundle.getString("actionid");
			String result = bundle.getString("result");
			long timestamp = bundle.getLong("timestamp");

			Log.d("GetuiSdkDemo", "appid = " + appid);
			Log.d("GetuiSdkDemo", "taskid = " + taskid);
			Log.d("GetuiSdkDemo", "actionid = " + actionid);
			Log.d("GetuiSdkDemo", "result = " + result);
			Log.d("GetuiSdkDemo", "timestamp = " + timestamp);
			break;
		default:
			break;
		}
	}
	
	
	//显示通知栏
	public void showNotification(Context context,String content,Intent intentTo,String title) {
		manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification();
		notification.icon = R.drawable.ic_launcher;
		notification.when = System.currentTimeMillis();
		notification.tickerText = content;  //通知的内容
		notification.defaults = Notification.DEFAULT_SOUND; //通知的铃声
		notification.vibrate  = new long[]{0,1000};
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		int flag = new Random().nextInt();
		intentTo.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent pi = PendingIntent.getActivity(context, flag, intentTo, 0);
		if(title == null) {
			title = context.getString(R.string.app_name);
		} else {
			title = context.getString(R.string.app_name) + title; 
		}
		notification.setLatestEventInfo(context, title, content, pi);
		manager.notify(flag, notification);
	}
}
