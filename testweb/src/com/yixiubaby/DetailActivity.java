package com.yixiubaby;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.yixiubaby_ls.R;

public class DetailActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		WebView web = (WebView) findViewById(R.id.web);
		TextView detail= (TextView) findViewById(R.id.detail);
		Intent intent = getIntent();
		String url = intent.getStringExtra("url");
		String str = intent.getStringExtra("str");
		web.loadUrl(url);
		detail.setText(str);
		
		web.setWebViewClient(new WebViewClient() {

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return super.shouldOverrideUrlLoading(view, url);
			}
			
		});
		
		web.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String arg1, String arg2,
					String arg3, long arg4) {
				Uri uri = Uri.parse(url);  
	            Intent intent = new Intent(Intent.ACTION_VIEW, uri);  
	            startActivity(intent);  
			}
		});
	}

	
}
