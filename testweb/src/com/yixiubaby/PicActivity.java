package com.yixiubaby;

import java.io.Serializable;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.yixiubaby.util.AlbumHelper;
import com.yixiubaby.util.ImageBucket;
import com.yixiubaby.util.ImageBucketAdapter;
import com.yixiubaby_ls.R;

public class PicActivity extends Activity {
	// ArrayList<Entity> dataList;//用来装载数据源的列表
	List<ImageBucket> dataList;
	GridView gridView;
	ImageBucketAdapter adapter;// 自定义的适配器
	AlbumHelper helper;
	public static final String EXTRA_IMAGE_LIST = "imagelist";
	public static Bitmap bimap;
	
	String demo;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		intStatus(savedInstanceState);
		setContentView(R.layout.activity_image_bucket);
		Intent intent = getIntent();
		demo = intent.getStringExtra("demo");
		
		helper = AlbumHelper.getHelper();
		helper.init(getApplicationContext());
		initData();
		initView();
	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		dataList = helper.getImagesBucketList();	
		bimap=BitmapFactory.decodeResource(getResources(),R.drawable.icon_addpic_unfocused);
	}

	/**
	 * 初始化view视图
	 */
	private void initView() {
		gridView = (GridView) findViewById(R.id.gridview);
		adapter = new ImageBucketAdapter(PicActivity.this, dataList);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Intent intent = new Intent(PicActivity.this,ImageGridActivity.class);
				intent.putExtra(PicActivity.EXTRA_IMAGE_LIST,(Serializable) dataList.get(position).imageList);
				intent.putExtra("demo", demo);
				startActivityForResult(intent,1);
			}

		});
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == 1 && resultCode == 1) {
			finish();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		intStatus(savedInstanceState);
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	public void intStatus(Bundle savedInstanceState) {
		if(savedInstanceState != null) {
			demo = savedInstanceState.getString("demo");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(demo != null) {
			outState.putString("demo", demo);
		}
		super.onSaveInstanceState(outState);
	}
}
