package com.yixiubaby;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.baidu.android.pushservice.PushConstants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.umeng.analytics.MobclickAgent;
import com.yixiubaby_ls.R;

public class MainActivity extends Activity {
	Handler handler = new Handler();
	private static final String MEDIA = "media";
	private static final int STREAM_VIDEO = 5;

	// String url = "http://jyt.mingxiaocn.com/";
	String weburl = "http://3g.mingxiaocn.com";

	protected ImageLoader imageLoader = ImageLoader.getInstance();
	private static final int PHOTO_REQUEST_TAKEPHOTO = 1;// 拍照
	private static final int PHOTO_REQUEST_GALLERY = 2;// 从相册中选择
	String[] strs = new String[] { "拍照", "相册" };
	File tempFile = new File(Environment.getExternalStorageDirectory()
			+ "/mx", getPhotoFileName());

	// 使用系统当前日期加以调整作为照片的名称
	@SuppressLint("SimpleDateFormat")
	private String getPhotoFileName() {
		Date date = new Date(System.currentTimeMillis());
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"'IMG'yyyyMMddHHmmss");
		return dateFormat.format(date) + ".jpg";
	}
	
	String demo;
	WebView web;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		intStatus(savedInstanceState);
		setContentView(R.layout.activity_main);
		// PushManager.getInstance().initialize(this.getApplicationContext());
		
		com.baidu.android.pushservice.PushManager.startWork(
				getApplicationContext(),
				PushConstants.LOGIN_TYPE_API_KEY,
				Utils.getMetaValue(MainActivity.this, "api_key"));
		web = (WebView) findViewById(R.id.web);
		web.getSettings().setJavaScriptEnabled(true);
		web.loadUrl(weburl);
		
		web.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageFinished(WebView view, String url) {
				CookieManager cm = CookieManager.getInstance();
				String cookie = cm.getCookie(url);
				System.err.println(cookie + ":cookie");
				if(cookie != null) {
					if(cookie.contains("loginName") && cookie.contains("Code")) {
						String[] splits = cookie.split("&");
						String Code = null;
						String loginName = null;
						if(splits != null && splits.length > 0) {
							for(int i = 0; i < splits.length; i++) {
								String str = splits[i];
								System.err.println(str);
								if(str.contains("Code")) {
									if(str.length() > 4) {
										Code = str.substring(5, str.length());
										System.err.println("Code:" + Code);
									}
								} else if(str.contains("loginName")) {
									if(str.length() > 9) {
										loginName = str.substring(10, str.length());
										System.err.println("loginName:" + loginName);
									}
								}
							}
						}
						if(Code != null && loginName != null) {
							BaseApplication.userID = Code + loginName;
							List<String> tags = new ArrayList<String>();
							if(BaseApplication.userID != null) {
								tags.add(BaseApplication.userID);
								com.baidu.android.pushservice.PushManager.setTags(getApplicationContext(), tags);
							}
						}
					}
				}
				super.onPageFinished(view, url);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				view.loadUrl(url);
				return super.shouldOverrideUrlLoading(view, url);
			}

		});
		
		web.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String arg1, String arg2,
					String arg3, long arg4) {
				Uri uri = Uri.parse(url);  
	            Intent intent = new Intent(Intent.ACTION_VIEW, uri);  
	            startActivity(intent);  
			}
		});
		
		web.addJavascriptInterface(new Object() {
			@SuppressWarnings("unused")
			@JavascriptInterface
			public void startplay(final String str, final int type) {
				handler.post(new Runnable() {
					@Override
					public void run() {
						Intent intent = new Intent(MainActivity.this,MediaPlayerActivity.class);
						intent.putExtra(MEDIA, STREAM_VIDEO);
						intent.putExtra("url", str);
						startActivity(intent);
					}
				});
			}
			
			@SuppressWarnings("unused")
			@JavascriptInterface
			public void upload(String demo) {
				MainActivity.this.demo = demo;
				handler.post(new Runnable() {
					@Override
					public void run() {
						showDialog();
					}
				});
			}
			
			@JavascriptInterface
		    public void getversion() {
		      	handler.post(new Runnable() {  
		      		@Override  
		      		public void run() { 
		                int version = getVersion(MainActivity.this);
		                web.loadUrl("javascript:getVersion('" + version + "')");
		            }  
		      	});
		    }

			
		}, "obj");

		new Wether().execute();
	}
	
	// 获取apk版本号
	public static int getVersion(Context context) {
		int version = 1;
		PackageManager pm = context.getPackageManager();
		try {
			PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
			version = pi.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return version;
	}
	
	@Override
	protected void onResume() {
		MobclickAgent.onResume(this);
		BaseApplication app = (BaseApplication) getApplication();
		if(app.sc && app.urls != null) {
			String js = "javascript:Submit_Success('" + app.urls.replaceAll("\r", "").replaceAll("\n", "") + "')";
			 web.loadUrl("javascript:Submit_Success('" + app.urls.replaceAll("\r", "").replaceAll("\n", "") + "')");
			 app.sc = false;
			 app.urls = null;
		}
		super.onResume();
	}

	@Override
	protected void onPause() {
		MobclickAgent.onPause(this);
		super.onPause();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		intStatus(savedInstanceState);
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	public void intStatus(Bundle savedInstanceState) {
		if(savedInstanceState != null) {
			demo = savedInstanceState.getString("demo");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(demo != null) {
			outState.putString("demo", demo);
		}
		super.onSaveInstanceState(outState);
	}

	public void showDialog() {
		new AlertDialog.Builder(MainActivity.this)
				.setItems(strs, new OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.dismiss();
						if (arg1 == 0) {
							// 调用系统的拍照功能
							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
							// 指定调用相机拍照后照片的储存路径
//							intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(tempFile));
//							intent.putExtra("android.intent.extra.quickCapture",true);
							startActivityForResult(intent,PHOTO_REQUEST_TAKEPHOTO);
							
//							Intent in = new Intent(MainActivity.this, TakePicActivity.class);
//							startActivity(in);
						} else {
							toPic();
						}
					}
				}).create().show();
	}
	
	public void toPic() {
		Intent intent = new Intent(MainActivity.this, PicActivity.class);
		intent.putExtra("demo", demo);
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		toPic();
		switch (requestCode) {
		case PHOTO_REQUEST_TAKEPHOTO:
			if(data != null) {
				Uri uri = data.getData();
				Intent intent = new Intent(MainActivity.this, TakePicActivity.class);
				intent.putExtra("uri", uri.toString());
				intent.putExtra("demo", demo);
				startActivity(intent);
			}
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	public File getAbsoluteImagePath(Uri uri) {
		// can post image
		String[] proj = { MediaStore.Images.Media.DATA };
		@SuppressWarnings("deprecation")
		Cursor cursor = managedQuery(uri, proj, // Which columns to return
				null, // WHERE clause; which rows to return (all rows)
				null, // WHERE clause selection arguments (none)
				null); // Order-by clause (ascending by name)

		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return new File(cursor.getString(column_index));
	}

	public void loadImg(final String uri, final boolean update) {
		imageLoader.loadImage(uri, BaseApplication.options,
		new ImageLoadingListener() {
			@Override
			public void onLoadingStarted(String imageUri, View view) {
			}

			@Override
			public void onLoadingFailed(String imageUri, View view,
					FailReason failReason) {
			}

			@Override
			public void onLoadingComplete(String imageUri, View view,
					Bitmap loadedImage) {
				if (loadedImage != null) {
				}
			}

			@Override
			public void onLoadingCancelled(String imageUri, View view) {
			}
		});
	}
	
	public void click(View view) {
		showDialog();
	}
}
