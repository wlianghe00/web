package com.yixiubaby;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.http.Header;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.yixiubaby.util.AlbumHelper;
import com.yixiubaby.util.CommonProgressDialog;
import com.yixiubaby.util.ImageGridAdapter;
import com.yixiubaby.util.ImageGridAdapter.TextCallback;
import com.yixiubaby.util.ImageItem;
import com.yixiubaby_ls.R;

public class ImageGridActivity extends PhotoActivity {
	public static final String EXTRA_IMAGE_LIST = "imagelist";

	List<ImageItem> dataList;
	GridView gridView;
	ImageGridAdapter adapter;
	AlbumHelper helper;
	Button bt;
	
	String demo;
	AsyncHttpClient client = new AsyncHttpClient();
	CommonProgressDialog dialog;

	@SuppressLint("HandlerLeak")
	Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case 0:
				Toast.makeText(ImageGridActivity.this, "最多选择" + ImageGridAdapter.SUM + "张图片", 400).show();
				break;

			default:
				break;
			}
		}
	};

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		intStatus(savedInstanceState);
		
		setContentView(R.layout.activity_image_grid);
		Intent intent = getIntent();
		demo = intent.getStringExtra("demo");
		
		helper = AlbumHelper.getHelper();
		helper.init(getApplicationContext());
		dataList = (List<ImageItem>) getIntent().getSerializableExtra(EXTRA_IMAGE_LIST);
		initView();
		bt = (Button) findViewById(R.id.bt);
		bt.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(adapter.map.size() > 0) {
					getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
					dialog = new CommonProgressDialog(ImageGridActivity.this, client);
					dialog.show();
					dialog.setTip("提交中...");
					new Thread(new Runnable() {
						@Override
						public void run() {
							RequestParams params = new RequestParams();
							params.put("demo", demo);
							Collection<String> values = adapter.map.values();
							Iterator<String> it = values.iterator();
							int i = 0;
							while(it.hasNext()) {
								String url = it.next();
								File file = new File(url);
								try {
									InputStream is = getSmallBitmap(Uri.fromFile(file));
									params.put("file" + i, is);
									i++;
								} catch (FileNotFoundException e1) {
									e1.printStackTrace();
								}
							}
							//http://121.42.40.50:8080/sc/servlet/Up
							new SyncHttpClient().post("http://121.42.40.50:8080/sc/servlet/Up", params, new AsyncHttpResponseHandler() {
								@Override
								public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
									String str = new String(responseBody);
									((BaseApplication)getApplication()).sc = true;
									((BaseApplication)getApplication()).urls = str;
									setResult(1);
									finish();
//									if(str.replace("\n", "").replace("\r", "").equals("200")) {
//										
//									} else {
//										runOnUiThread(new Runnable() {
//											@Override
//											public void run() {
//												Toast.makeText(ImageGridActivity.this, "网络错误,上传失败", Toast.LENGTH_SHORT).show();
//											}
//										});
//									}
								}
								
								@Override
								public void onFailure(int statusCode, Header[] headers,
										byte[] responseBody, Throwable error) {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											Toast.makeText(ImageGridActivity.this, "网络错误,上传失败", Toast.LENGTH_SHORT).show();
										}
									});
								}

								@Override
								public void onFinish() {
									runOnUiThread(new Runnable() {
										@Override
										public void run() {
											getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
											if(dialog != null) {
												dialog.dismiss();
											}
										}
									});
									super.onFinish();
								}
								
							});
						}
					}).start();
					
				} else {
					Toast.makeText(ImageGridActivity.this, "请选择照片", Toast.LENGTH_SHORT).show();
				}
				
				
			}

		});
	}

	/**
	 */
	private void initView() {
		gridView = (GridView) findViewById(R.id.gridview);
		gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
		adapter = new ImageGridAdapter(ImageGridActivity.this, dataList,mHandler);
		gridView.setAdapter(adapter);
		adapter.setTextCallback(new TextCallback() {
			public void onListen(int count) {
				bt.setText("完成" + "(" + count + ")");
			}
		});

		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				adapter.notifyDataSetChanged();
			}

		});

	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		intStatus(savedInstanceState);
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	public void intStatus(Bundle savedInstanceState) {
		if(savedInstanceState != null) {
			demo = savedInstanceState.getString("demo");
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		if(demo != null) {
			outState.putString("demo", demo);
		}
		super.onSaveInstanceState(outState);
	}
	
}
