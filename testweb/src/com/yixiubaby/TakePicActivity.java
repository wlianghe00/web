package com.yixiubaby;

import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.http.Header;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.yixiubaby.util.CommonProgressDialog;
import com.yixiubaby_ls.R;


public class TakePicActivity extends PhotoActivity {
	String demo;
	int screenWidth,screenHeight;
	
	ImageLoader imageLoader = ImageLoader.getInstance();
	AsyncHttpClient client = new AsyncHttpClient();
	CommonProgressDialog dialog;
	String uri;
	
	ImageView iv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_picture);
		Intent intent = getIntent();
		demo = intent.getStringExtra("demo");
		uri = intent.getStringExtra("uri");
		
		iv = (ImageView) findViewById(R.id.image);
		imageLoader.displayImage(uri, iv, BaseApplication.options,new ImageLoadingListener() {
			@Override
			public void onLoadingStarted(String arg0, View arg1) {
			}
			@Override
			public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
			}
			@Override
			public void onLoadingComplete(String arg0, View arg1, Bitmap bitmap) {
				if(bitmap != null) {
					Uri u = Uri.parse(uri);
					int degree = readPictureDegree(getAbsoluteImagePath(u).getAbsolutePath());  
					bitmap = rotaingImageView(degree, bitmap);
					iv.setImageBitmap(bitmap);
				}
			}
			@Override
			public void onLoadingCancelled(String arg0, View arg1) {
			}
		});
	}
	
	public void back(View view) {
		finish();
	}

	public void commit(View view) {
		getWindow().addFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		dialog = new CommonProgressDialog(TakePicActivity.this, client);
		dialog.show();
		dialog.setTip("�ύ��...");
		new Thread(new Runnable() {
			@Override
			public void run() {
				RequestParams params = new RequestParams();
				params.put("demo", demo);
				if(uri != null) {
					try {
						InputStream is = getSmallBitmap(Uri.parse(uri));
						params.put("file", is);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
				}
				new SyncHttpClient().post("http://121.42.40.50:8080/sc/servlet/Up", params, new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
						String str = new String(responseBody);
						setResult(1);
						finish();
						((BaseApplication)getApplication()).sc = true;
						((BaseApplication)getApplication()).urls = str;
						
//						if(str.replace("\n", "").replace("\r", "").equals("200")) {
//						} else {
//							runOnUiThread(new Runnable() {
//								@Override
//								public void run() {
//									Toast.makeText(TakePicActivity.this, "�������,�ϴ�ʧ��", Toast.LENGTH_SHORT).show();
//								}
//							});
//						}
					}
					
					@Override
					public void onFailure(int statusCode, Header[] headers,
							byte[] responseBody, Throwable error) {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(TakePicActivity.this, "�������,�ϴ�ʧ��", Toast.LENGTH_SHORT).show();
							}
						});
					}

					@Override
					public void onFinish() {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
								if(dialog != null) {
									dialog.dismiss();
								}
							}
						});
						super.onFinish();
					}
					
				});
			}
		}).start();
	}
}
