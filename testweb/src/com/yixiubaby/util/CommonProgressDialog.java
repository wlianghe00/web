package com.yixiubaby.util;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.yixiubaby_ls.R;

/**
 * 通用进度�?
 * @author wlh
 */
public class CommonProgressDialog extends Dialog {
	AsyncHttpClient client;
	TextView tv_tip;

	public CommonProgressDialog(Context context, boolean cancelable,OnCancelListener cancelListener) {
		super(context, cancelable, cancelListener);
	}

	public CommonProgressDialog(Context context, int theme) {
		super(context, theme);
	}

	public CommonProgressDialog(final Context context,final AsyncHttpClient client) {
		super(context, R.style.MyDialog);
		this.client = client;
		setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if(client != null) {
					client.cancelRequests(context, true);
				}
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.common_progress);
		tv_tip = (TextView) findViewById(R.id.tv_tip);
	}

	//设置提示
	public void setTip(String text) {
		tv_tip.setText(text);
	}
}
